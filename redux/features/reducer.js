import { combineReducers } from 'redux';
import counterReducer from './counter/counter-slice';

export default combineReducers({
  counterReducer,
});
