import Head from 'next/head';

const _keywords = 'maybe, booking';
const _description =
    'maybe booking';
const _company = 'Công ty Cổ phần Maybe';

const HeadMeta = ({ title, description, keywords, path, image, canonical }) => {
    const _thumbnail = image ? image : `${process.env.NEXT_PUBLIC_APP_URL}/static/images/image-default.jpg`;
    const _canonical = canonical ? `${process.env.NEXT_PUBLIC_APP_URL}${canonical}` : process.env.NEXT_PUBLIC_APP_URL;

    return (
        <Head>
            <link rel="icon" href="/favicon.ico" />
            <link rel="canonical" href={_canonical} />
            <title>{title ? title : process.env.NEXT_PUBLIC_APP_NAME}</title>
            <meta name="keywords" content={keywords ? _keywords + ', ' + keywords : _keywords} />
            <meta name="description" content={description ? description : _description} />
            <meta name="author" content={'Maybe'} />
            <meta name="copyright" content={_company} />
            <meta name="thumbnail" content={_thumbnail} />

            <meta itemProp="name" content={title ? title : process.env.NEXT_PUBLIC_APP_NAME} />
            <meta itemProp="image" content={_thumbnail} />

            <meta name="twitter:card" content="article" />
            <meta name="twitter:site" content={process.env.NEXT_PUBLIC_APP_NAME} />
            <meta name="twitter:title" content={title ? title : process.env.NEXT_PUBLIC_APP_NAME} />
            <meta name="twitter:description" content={description ? description : _description} />
            <meta name="twitter:creator" content={_company} />
            <meta name="twitter:image" content={_thumbnail} />

            <meta property="og:type" content="article" />
            <meta property="og:site_name" content={process.env.NEXT_PUBLIC_APP_NAME} />
            <meta property="og:title" content={title ? title : process.env.NEXT_PUBLIC_APP_NAME} />
            <meta property="og:description" content={description ? description : _description} />
            <meta property="og:url" content={`${path ? path : process.env.NEXT_PUBLIC_APP_URL}`} />
            <meta property="og:image" content={_thumbnail} />
            <meta property="og:image:secure_url" content={_thumbnail} />
            <meta property="og:locale" content="vi_VN" />
            <meta property="fb:app_id" content={process.env.NEXT_PUBLIC_FACEBOOK_CLIENT_ID} />
        </Head>
    );
};

export default HeadMeta;
