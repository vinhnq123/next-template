import React, { useEffect, useState } from 'react';


export default function Dropdown() {
  const [activeNav, setActiveNav] = useState(false);

  return (
    <>
      <div className="flex flex-wrap z-10">
        <div className="w-full sm:w-6/12 md:w-4/12">
          <div className="relative inline-flex align-middle w-full">
            <button
              className="bg-blue-500 text-white active:bg-blue-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button" onClick={() => setActiveNav(!activeNav)} >
              Demo dropdown
            </button>
            <div
              className={"absolute bg-white left-0 top-full text-base z-50 float-left py-2 list-none text-left rounded shadow-lg mt-1" + (activeNav ? ' block' : ' hidden')}>

              <a
                href="#pablo"
                className=
                "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent "

              >
                Action
              </a>
              <a
                href="#pablo"
                className=
                "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent"
              >
                Another action
              </a>
              <a
                href="#pablo"
                className=
                "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent "


              >
                Something else here
              </a>
              <div className="h-0 my-2 border border-solid border-t-0 border-slate-800 opacity-25" />
              <a
                href="#pablo"
                className=
                "text-sm py-2 px-4 font-normal block w-full whitespace-nowrap bg-transparent "

              >
                Seprated link
              </a>
            </div>
          </div>
        </div>
      </div>
      
    </>
  );
}

