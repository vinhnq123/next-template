import { _str, serialize } from "@/common/Support/helpers";
import { INIT_REQUEST, httpClient, sendResponse } from "@/lib/api";

export default {
  getList(params = INIT_REQUEST) {
    const { req, token, query } = params;
    const searchParams = serialize(query);

    return httpClient(req, token)
      .get(_str("/users").append(serialize(searchParams)).get())
      .then((res) => sendResponse(res));
  },
};
