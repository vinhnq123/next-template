import { useCallback, useEffect, useState } from "react";
import { INIT_PAGINATION } from "@/common/utils";
import userService from "@/services/userService";
import { _obj } from "@/common/Support/helpers";

export const useUsers = (initUsers = [], initPagination = INIT_PAGINATION) => {
  const [users, setUsers] = useState(initUsers);
  const [pagination, setPagination] = useState(initPagination);

  useEffect(() => {
    initUsers.length === 0 && fetchUsers();
  }, []);

  const fetchUsers = useCallback(query => {
    userService.getList({
      query,
      token: '',
    }).then(res => {
      if (res?.status === 200) {
        setUsers(_obj.get(res, 'data.users', []));
        setPagination(_obj.get(res, 'data.meta', {...INIT_PAGINATION}));
      }
    }).catch(err => console.error(err.response));
  }, []);

  return { users, pagination, fetchUsers };
};
