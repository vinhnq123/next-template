import axios from "axios";

const baseURL = process.env.NEXT_PUBLIC_API_URL + "/api";
const proxyURL = process.env.NEXT_PUBLIC_APP_URL + "/next-api";

const REQUEST_HEADERS = {
  "Content-Type": "application/json",
  "Accept": "application/json",
  "X-Requested-With": "XMLHttpRequest",
  "Cache-Control": "no-store",
};

const INIT_REQUEST = {
  data: null,
  path: null,
  query: null,
  req: null,
  token: null,
};

const getOptionCreateAxios = (req) => {
  const headers = { ...REQUEST_HEADERS };
  const options = {
    baseURL: req ? baseURL : proxyURL,
    headers,
  };
  return options;
};

const httpClient = (req, token) => {
  const instance = axios.create(getOptionCreateAxios(req));
  instance.interceptors.request.use((config) => {
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  });

  instance.interceptors.response.use(
    (response) => response,
    (error) => Promise.resolve({ error })
  );

  return instance;
};

const sendResponse = (res, withError = false) =>
  new Promise((resolve, reject) => {
    if (res.error) {
      withError ? reject(res.error.response) : resolve(res.error.response);
    } else {
      resolve(res);
    }
  });

export { REQUEST_HEADERS, INIT_REQUEST, httpClient, sendResponse };
