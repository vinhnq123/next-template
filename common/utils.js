export const INIT_PAGINATION = {
  current_page: 1,
  from: 1,
  last_page: 1,
  per_page: 10,
  to: 1,
  total: 1,
  links: [],
};
