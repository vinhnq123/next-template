import Str from './Str';
import Arr from './Arr';

/**
 * Kiểm tra 1 giá trị nào đó có tồn tại hay không.
 * @param {mixed} value Giá trị cần kiểm tra
 * @returns bool
 */
export const isset = value => {
    return value !== undefined && value !== null;
};

/**
 * Kiểm tra 1 giá trị nào đó có rỗng hay không.
 * @param {mixed} value Giá trị cần kiểm tra
 * @returns bool
 */
export const empty = value => {
    if (Array.isArray(value)) {
        return value.length === 0;
    }
    return value === undefined || value === null || value === false || value === '' || value === 0;
};

/**
 * Sao chép link.
 * @param SyntheticBaseEvent e
 * @param Object options
 */
export function copyLink(e, options = { url: null, callback: null }) {
    const temp = document.createElement('input');
    document.body.appendChild(temp);
    temp.value = _obj.get(options, 'url', window.location.href);
    temp.focus();
    temp.select();
    navigator.clipboard.writeText(temp.value);
    temp.parentNode.removeChild(temp);
    _obj.get(options, 'callback', () => alert('Đã sao chép liên kết.'))();
}

export const _obj = {
    combine: (keys, values) => {
        if (keys.length < values.length) {
            for (let i = 0; i < values.length - keys.length; i++) {
                keys.push(`key_${i}`);
            }
        }
        return keys.reduce(
            (pre, cur, curIndex) => ({
                ...pre,
                [cur]: values?.[curIndex] ? values[curIndex] : null,
            }),
            {}
        );
    },
    get: (obj, keys, defaultValue = null) => {
        let result = obj;
        keys.split('.').forEach(key => {
            result = result?.[key];
        });
        if (!isset(result)) {
            if (defaultValue instanceof Function) return defaultValue();
            return defaultValue !== undefined ? defaultValue : null;
        }
        return result;
    },
    only: (obj, list) => {
        return obj
            ? Object.keys(obj).reduce((pre, cur) => {
                  if (typeof list === 'string' && cur === list) {
                      return { ...pre, [cur]: obj[cur] };
                  } else if (Array.isArray(list) && list.includes(cur)) {
                      return { ...pre, [cur]: obj[cur] };
                  }
                  return { ...pre };
              }, {})
            : {};
    },
    except: (obj, list) => {
        return obj
            ? Object.keys(obj).reduce((pre, cur) => {
                  if (typeof list === 'string' && cur !== list) {
                      return { ...pre, [cur]: obj[cur] };
                  } else if (Array.isArray(list) && !list.includes(cur)) {
                      return { ...pre, [cur]: obj[cur] };
                  }
                  return { ...pre };
              }, {})
            : {};
    },
    map: (obj, callback) => {
        const result = [];
        for (const [key, value] of Object.entries(obj)) {
            result.push(callback(value, key));
        }
        return result;
    },
};

/**
 * Chuyển object thành query string.
 * @param {object} value Object cần chuyển đổi
 * @returns string
 */
export const serialize = value => {
  const urlSearchParams = new URLSearchParams();
  for (const key in data) {
    if (!isset(data[key]) || value === '') {
      continue;
    }

    if (typeof data[key] === 'object') {
      const entries = Object.entries(data[key]);

      for (const [field, value] of entries) {
        if (!isset(value) || value === '') {
          continue;
        }
        urlSearchParams.append(`${key}[${field}]`, value);
      }
    } else {
      urlSearchParams.append(key, data[key]);
    }
  }

  const result = urlSearchParams.toString();

  return empty(result) ? '' : `?${result}`;
};

/**
 * Chuyển query string sang object.
 * @param {string} value string cần chuyển đổi
 * @returns object
 */
export const queryStringToObject = value => {
  const urlSearchParams = new URLSearchParams(value);
  const entries = urlSearchParams.entries();
  const result = {};
  for (const [key, value] of entries) {
    result[key] = value;
  }
  return result;
};

/**
 * Object hỗ trợ xử lý chuỗi.
 */
export const _str = (value = '') => new Str(value);

/**
 * Khởi tạo đối tượng Arr.
 * @param {string} value Giá trị đối tượng cần hỗ trợ
 * @return Arr
 */
export const _arr = value => new Arr(value);

/**
 * Tạo ra 1 chuỗi ngẫu nhiên.
 * @param {int} length Độ dài chuỗi
 * @param {object} options Cấu hình điều kiện để tạo chuỗi
 * @returns string
 */
export const strRandom = (length = 16, options = {}) => new Str().random(length, options);
