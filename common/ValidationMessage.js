export default {
  showErrorMessage: options => {
    const { type, field, min, max, minLength, maxLength, regex } = options;
    switch (type) {
      case 'required':
        return `The ${field} field is required.`;
      case 'min':
        return `The ${field} must be at least ${min}.`;
      case 'max':
        return `The ${field} must not be greater than ${max}.`;
      case 'minLength':
        return `The ${field} must be at least ${minLength}.`;
      case 'maxLength':
        return `The ${field} must not be greater than ${maxLength}.`;
      case 'pattern':
        return regex;
      default:
        return '';
    }
  },
};
