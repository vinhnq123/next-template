
import Footer from "@/components/Shared/Footer";
import HeadMeta from "@/components/Shared/HeadMeta";
import Header from "@/components/Shared/Header";
import Main from "@/components/Shared/Main";

export default function Home() {
  return (
    <>
      <HeadMeta title="Trang chủ" description="Landing page" />
      <Header />
      <Main />
      <Footer />
    </>
  )
}
