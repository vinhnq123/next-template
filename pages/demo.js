
import HeadMeta from "@/components/Shared/HeadMeta";
import Accordion from "@/components/demo/Accordion ";
import Dropdown from "@/components/demo/Dropdown";
import Menu from "@/components/demo/Menu";
import Modal from "@/components/demo/Modal";
import Tab from "@/components/demo/Tab";

export default function Home() {
  return (
    <>
      <HeadMeta title="Trang chủ" description="Landing page" />
      <Menu />
      <div className="p-4 w-full">
        
        
        <Modal />
        <Tab color="blue"/>
       
        <Dropdown color="black" />
        <Accordion/>
      </div>


    </>
  )
}
